import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import PythonData from './PythonData'

ReactDOM.render(<PythonData />, document.getElementById('root'));
registerServiceWorker();
