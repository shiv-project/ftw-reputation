import React, {Component, Fragment} from 'react';
import axios from 'axios';

class PythonData extends Component {
    state = {
        result: null
    };

    componentDidMount() {
        axios.get('http://localhost:5000/python')
            .then(resposnse => {
                this.setState({result: resposnse.data});
            })
            .catch(error => {
                console.log(error.response)
            })
    }

    render() {
        if (this.state.result) {
            return (
                <div className="container">
                    <table className="text-center text-dark table-hover table script-data table-bordered table-striped">
                        <thead className="text-white">
                        <tr>
                            <th>Seller</th>
                            <th>Seller Rating</th>
                            <th>Product</th>
                            <th>Product Rating</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.result.map(row => {
                                return (
                                    <tr>
                                        <td>{row.name}</td>
                                        <td>{row.rating}</td>
                                        <td>{row.product.name}</td>
                                        <td>{row.product.rating}</td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                    </table>
                </div>
            )
        } else {
            return (
                <div>
                    <p>Loading...</p>
                </div>
            )
        }
    }
}

export default PythonData;
