pragma solidity >=0.4.22 <0.6.0;

contract TrustGraph {

    mapping (address => uint) public nodes;
    address[] public nodeList;

    uint public numNodes;

    uint[] public trusterArr;
    uint[] public trusteeArr;
    uint[] public ratingArr;

    uint[] public buyerArr;
    uint[] public senderArr;

    event Rating(uint trusterIndex, uint trusteeIndex, uint rating);
    //event Transfer(uint buyerIndex, uint senderIndex, uint rating);

    constructor () public {

        numNodes = 0;

    }

    function addNode(address addr) public returns(uint) {

        //require(nodes[node] != 0);

        nodeList.push(addr);
        numNodes++;
        nodes[addr] = numNodes;
        return numNodes;

    }

    function addEdge(address trustee, uint rating) public {

        address truster = msg.sender;

        require(truster != trustee);

        uint trusterIndex = nodes[truster];
        uint trusteeIndex = nodes[trustee];

        if (trusterIndex == 0) {
            trusterIndex = addNode(truster);
        }

        if (trusteeIndex == 0) {
            trusteeIndex = addNode(trustee);
        }

        trusterArr.push(trusterIndex - 1);
        trusteeArr.push(trusteeIndex - 1);
        ratingArr.push(rating);

        emit Rating(trusterIndex - 1, trusteeIndex - 1, rating);

    }


    function transferRating(address buyer, uint rating) public {

        address sender = msg.sender;

        require(buyer != sender);

        uint buyerIndex = nodes[buyer];
        uint senderIndex = nodes[sender];

        require(ratingArr[senderIndex-1] <= rating);

        if (buyerIndex == 0) {
            buyerIndex = addNode(buyer);
        }

        if (senderIndex == 0) {
            senderIndex = addNode(sender);
        }

        buyerArr.push(buyerIndex - 1);
        senderArr.push(senderIndex - 1);

        ratingArr[buyerIndex -1] + rating;
        ratingArr[senderIndex -1] - rating;

        //Transfer(buyerIndex - 1, senderIndex - 1, rating);

    }

    function getNodeList() public view returns(address[] memory) {
        return nodeList;
    }

    function getTrusterList() public view returns(uint[] memory) {

        return trusterArr;
    }

    function getTrusteeList() public view returns(uint[] memory) {

        return trusteeArr;
    }

    function getRatingList() public  view returns(uint[] memory) {

        return  ratingArr;
    }

    // function getEdgeList() public constant returns(uint[], uint[], uint[]) {

    //     return (trusterArr, trusteeArr, ratingArr);
    // }
}
